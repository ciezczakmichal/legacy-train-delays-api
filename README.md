# train-delays-web-api

## Uruchomienie serwera

```
npm run dev
```

Wsparcie "import" z ES6 działa dzięki paczce "esm" oraz dodaniu flagi "-r esm" do binarek node / nodemon.

Uruchamianie bez nodemon: node -r esm src/index.js

https://timonweb.com/tutorials/how-to-enable-ecmascript-6-imports-in-nodejs/
https://www.npmjs.com/package/esm
