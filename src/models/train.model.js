/**
 * @file Plik zawierający definicję modelu {@link Train}
 * @author Michał Ciężczak
 * @version 1.0.0
 */

import { Carrier } from './carrier.model'

/**
 * Klasa reprezentująca pojedynczy pociąg.
 */
class Train {
    /**
     * Tworzy instancję klasy z użyciem przekazanych wartości do inicjalizacji
     * prywatnych pól.
     * @param {number} id Unikalne ID pociągu
     * @param {Carrier} carrier Obiekt przewoźnika kolejowego, który uruchamia pociąg
     * @param {string} trainNumber Numer pociągu (może zawierać znak "/")
     * @param {string} trainName Nazwa pociągu (jeśli nie występuje - wartość pusta)
     * @param {string} routeString Pełna relacja pociągu (stacja początkowa i końcowa)
     * @param {TrainStationData[]} stationsDataArray Tablica danych poszczególnych stacji pociągu
     */
    constructor(
        id,
        carrier,
        trainNumber,
        trainName,
        routeString,
        stationsDataArray
    ) {
        if (!(carrier instanceof Carrier)) {
            throw new Error('Przewoźnik - wymagany obiekt klasy')
        }

        if (!Array.isArray(stationsDataArray)) {
            throw new Error(
                'Argument stationsDataArray - wymagana tablica danych stacji'
            )
        }

        this.id = id
        this.carrier = carrier
        this.trainNumber = trainNumber
        this.trainName = trainName
        this.routeString = routeString
        this.stationsData = stationsDataArray
    }

    /**
     * Zwraca ID pociągu przekazane do konstruktora.
     * @returns {number} ID pociągu
     */
    getId() {
        return this.id
    }

    /**
     * Zwraca instancję klasy przewoźnika kolejowego, który uruchamia
     * to połączenie kolejowe.
     * @returns {Carrier} Obiekt przewoźnika kolejowego
     */
    getCarrier() {
        return this.carrier
    }

    /**
     * Zwraca numer pociągu oznaczany w systemie kolejowym.
     * Wartość może zawierać znak "/" występujący w tego typu danych.
     * @returns {string} Numer pociągu
     */
    getTrainNumber() {
        return this.trainNumber
    }

    /**
     * Zwraca nazwę połączenia kolejowego.
     * Może to być wartość pusta.
     * @returns {string} Nazwa pociągu
     */
    getTrainName() {
        return this.trainName
    }

    /**
     * Zwraca ciąg tekstowy opisujący pełną relację pociągu, np.
     * Przemyśl Główny - Kraków Główny.
     * @returns {string} Relacja pociągu
     */
    getRouteString() {
        return this.routeString
    }

    /**
     * Zwraca tablicę zawierającą dane wszystkich stacji kolejowych na trasie pociągu.
     * @returns {TrainStationData[]} Tablica danych stacji
     */
    getStationsData() {
        return this.stationsData
    }

    /**
     * Zwraca dane rozkładowe pociągu dla pojedynczej, wskazanej stacji.
     * W przypadku podania nieprawidłowego ID metoda zwraca NULL.
     * @param {number} stationId ID stacji, dla której mają zostać pobrane dane
     * @returns {TrainStationData} Dane pociągu dla stacji
     */
    getDataForStation(stationId) {
        return (
            this.stationsData.find(data => data.getStationId() === stationId) ||
            null
        )
    }

    /**
     * Zwraca dane rozkładowe pociągu dla pojedynczej stacji za pomocą indeksu
     * w tablicy stacji. Funkcja wywołująca powinna upewnić się, że indeks
     * jest prawidłowy.
     * @param {number} index Indeks stacji do pobrania
     * @returns {TrainStationData} Dane pociągu dla stacji
     */
    getDataForStationIndex(index) {
        return this.stationsData[index]
    }

    /**
     * Zwraca całkowitą liczbę stacji na trasie pociągu.
     * @returns {number} Liczba stacji
     */
    stationsCount() {
        return this.stationsData.length
    }

    /**
     * Zwraca informację, czy stacja o wskazanym ID znajduje się w rozkładzie pociągu.
     * @param {number} stationId ID stacji do sprawdzenia
     * @returns {boolean} Wartość logiczna informująca o występowaniu
     */
    hasStationInRoute(stationId) {
        return this.getDataForStation(stationId) !== null
    }

    /**
     * Zwraca informację, czy pociąg rozpoczął już swój bieg
     * (wyjechał ze stacji początkowej).
     * @returns {boolean} Wartość wynikowa sprawdzenia
     */
    hasStartedRoute() {
        const firstStation = this.getDataForStationIndex(0)
        return firstStation.hasTrainAlreadyLeft()
    }

    /**
     * Zwraca informację, czy pociąg zakończył już swój bieg
     * (dotarł na stację końcową).
     * @returns {boolean} Wartość wynikowa sprawdzenia
     */
    hasFinishedRoute() {
        const lastStation = this.getDataForStationIndex(
            this.stationsCount() - 1
        )
        return lastStation.hasTrainAlreadyArrived()
    }
}

export { Train }
