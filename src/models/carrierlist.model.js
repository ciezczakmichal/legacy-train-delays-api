/**
 * @file Plik zawierający definicję modelu {@link CarrierList}
 * @author Michał Ciężczak
 * @version 1.0.0
 */

/**
 * Klasa reprezentująca bazę przewoźników kolejowych.
 */
class CarrierList {
    /**
     * Tworzy instancję klasy z użyciem przekazanych wartości do inicjalizacji
     * prywatnych pól.
     * @param {Carrier[]} carriers Tablica zawierająca listę przewoźników
     * @param {string} logoBaseUrl Adres URL, pod którym dostępne są loga przewoźników
     */
    constructor(carriers, logoBaseUrl) {
        this.carriers = carriers
        this.logoBaseUrl = logoBaseUrl
    }

    /**
     * Zwraca tablicę przewoźników przekazaną do konstruktora.
     * @returns {Carrier[]} Tablica przewoźników
     */
    getCarriers() {
        return this.carriers
    }

    /**
     * Zwraca adres URL, pod którym dostępne są loga przewoźników umieszczonych w obiekcie.
     * @returns {string} Adres URL
     */
    getLogoBaseUrl() {
        return this.logoBaseUrl
    }

    /**
     * Wyszukuje przewoźnika o wskazanym ID i zwraca go.
     * W przypadku, gdy nie udało się go odnaleźć, metoda zwraca NULL.
     * @param {number} carrierId ID przewoźnika do odnalezienia
     * @returns {Carrier|null} Obiekt wyszukiwanego przewoźnika lub NULL
     */
    findCarrier(carrierId) {
        return (
            this.carriers.find(carrier => carrier.getId() == carrierId) || null
        )
    }
}

export { CarrierList }
