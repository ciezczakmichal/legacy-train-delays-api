/**
 * @file Plik zawierający definicję modelu {@link TrainStationData}
 * @author Michał Ciężczak
 * @version 1.0.0
 */

import dayjs from 'dayjs'
import { getNowInMinutesForTargetTimezone } from '../helpers/date'

const timeStringFormat = 'HH:mm'

const convertTimeToMinutes = timeString => {
    const time = dayjs(timeString, timeStringFormat)

    if (!time.isValid()) {
        throw new Error('Nieudana konwersja czasu')
    }

    return time.hour() * 60 + time.minute()
}

const zeroTime = dayjs(new Date(2020, 1, 1))

const convertMinutesToTime = minutes => {
    const obj = zeroTime.minute(minutes)
    return obj.format(timeStringFormat)
}

const calculateDelay = (timeString, currentTimeString) => {
    const timeMinutes = convertTimeToMinutes(timeString)
    const currentTimeMinutes = convertTimeToMinutes(currentTimeString)

    return currentTimeMinutes - timeMinutes
}

/**
 * Klasa reprezentująca dane pociągu dla pojedynczej stacji kolejowej.
 */
class TrainStationData {
    /**
     * Tworzy instancję klasy z użyciem przekazanych wartości do inicjalizacji
     * prywatnych pól.
     * Czas podawany w argumentach powinien być zapisany jako string
     * w formacie HH:mm lub podany jako wartość NULL.
     *
     * @param {number} stationId ID stacji, której dane dotyczą
     * @param {string|null} arrivalTime Rozkładowy czas przyjazdu
     * @param {string|null} departureTime Rozkładowy czas odjazdu
     * @param {string|null} currentArrivalTime Bieżący czas przyjazdu
     * @param {string|null} currentDepartureTime Bieżący czas odjazdu
     */
    constructor(
        stationId,
        arrivalTime,
        departureTime,
        currentArrivalTime,
        currentDepartureTime
    ) {
        this.stationId = stationId
        this.arrivalTime = arrivalTime
        this.departureTime = departureTime
        this.currentArrivalTime = currentArrivalTime
        this.currentDepartureTime = currentDepartureTime
    }

    /**
     * Zwraca ID stacji, której dotyczą dane obiektu.
     * @returns {number} ID stacji kolejowej
     */
    getStationId() {
        return this.stationId
    }

    /**
     * Zwraca rozkładowy czas przyjazdu pociągu na stację jako string w formacie HH:mm.
     * W przypadku stacji początkowej wynikiem metody jest NULL.
     * @returns {string|null} Rozkładowy czas przyjazdu pociągu
     */
    getArrivalTime() {
        return this.arrivalTime
    }

    /**
     * Zwraca rozkładowy czas przyjazdu pociągu w postacji liczby minut, które
     * upływają od północy. W przypadku stacji początkowej wartością zwracaną jest NULL.
     * @returns {number|null} Rozkładowy czas przyjazdu jako liczba minut
     */
    getArrivalTimeInMinutes() {
        if (this.isFirstStation()) {
            return null
        } else {
            return convertTimeToMinutes(this.arrivalTime)
        }
    }

    /**
     * Zwraca rozkładowy czas odjazdu pociągu ze stacji jako string w formacie HH:mm.
     * W przypadku stacji końcowej wynikiem metody jest NULL.
     * @returns {string|null} Rozkładowy czas odjazdu pociągu
     */
    getDepartureTime() {
        return this.departureTime
    }

    /**
     * Zwraca rozkładowy czas odjazdu pociągu w postacji liczby minut, które
     * upływają od północy. W przypadku stacji końcowej wartością zwracaną jest NULL.
     * @returns {number|null} Rozkładowy czas odjazdu jako liczba minut
     */
    getDepartureTimeInMinutes() {
        if (this.isFinalStation()) {
            return null
        } else {
            return convertTimeToMinutes(this.departureTime)
        }
    }

    /**
     * Zwraca bieżący czas przyjazdu pociągu na stację jako string w formacie HH:mm.
     * W przypadku stacji początkowej wynikiem metody jest NULL.
     * Wartość różniąca się od wyniku metody {@link TrainStationData#getArrivalTime}
     * oznacza, że wystąpiły odchylenia od planowanego biegu pociągu.
     * @returns {string|null} Bieżący czas przyjazdu pociągu
     */
    getCurrentArrivalTime() {
        return this.currentArrivalTime
    }

    /**
     * Zwraca bieżący czas odjazdu pociągu ze stacji jako string w formacie HH:mm.
     * W przypadku stacji końcowej wynikiem metody jest NULL.
     * Wartość różniąca się od wyniku metody {@link TrainStationData#getDepartureTime}
     * oznacza, że wystąpiły odchylenia od planowanego biegu pociągu.
     * @returns {string|null} Bieżący czas odjazdu pociągu
     */
    getCurrentDepartureTime() {
        return this.currentDepartureTime
    }

    /**
     * Zwraca wartość opóźnienia pociągu w minutach dla czasu przyjazdu na stację.
     * Dla stacji początkowej wartością zwracaną jest 0.
     * @returns {number} Wartość opoźnienia
     */
    getArrivalDelay() {
        if (this.isFirstStation()) {
            return 0
        } else {
            return calculateDelay(this.arrivalTime, this.currentArrivalTime)
        }
    }

    /**
     * Zwraca wartość opóźnienia pociągu w minutach dla czasu odjazdu ze stacji.
     * Dla stacji końcowej wartością zwracaną jest 0.
     * @returns {number} Wartość opoźnienia
     */
    getDepartureDelay() {
        if (this.isFinalStation()) {
            return 0
        } else {
            return calculateDelay(this.departureTime, this.currentDepartureTime)
        }
    }

    /**
     * Przypisuje bieżący czas przyjazdu pociągu na stację.
     * Argument podawany jest w formie liczby minut.
     * @param {number} minutes Czas przyjazdu w minutach
     */
    setCurrentArrivalTimeInMinutes(minutes) {
        this.currentArrivalTime = convertMinutesToTime(minutes)
    }

    /**
     * Przypisuje bieżący czas odjazdu pociągu ze stacji.
     * Argument podawany jest w formie liczby minut.
     * @param {number} minutes Czas odjazdu w minutach
     */
    setCurrentDepartureTimeInMinutes(minutes) {
        this.currentDepartureTime = convertMinutesToTime(minutes)
    }

    /**
     * Zwraca informację, czy obiekt reprezentuje stację początkową trasy pociągu.
     * @returns {boolean} Wynik sprawdzenia
     */
    isFirstStation() {
        return this.arrivalTime === null
    }

    /**
     * Zwraca informację, czy obiekt reprezentuje stację końcową trasy pociągu.
     * @returns {boolean} Wynik sprawdzenia
     */
    isFinalStation() {
        return this.departureTime === null
    }

    /**
     * Zwraca informację, czy pociąg przyjechał już na stację (teraz lub w przeszłości).
     * Metoda uwzględnia aktualny czas przyjazdu pociągu na stację (tzn. potencjalne opóźnienie).
     * @returns {boolean} Wartość logiczna będąca wynikiem sprawdzenia
     */
    hasTrainAlreadyArrived() {
        if (this.isFirstStation()) {
            return this.hasTrainAlreadyLeft()
        }

        const nowMinutes = getNowInMinutesForTargetTimezone()
        const arrival = convertTimeToMinutes(this.getCurrentArrivalTime())
        return nowMinutes >= arrival
    }

    /**
     * Zwraca informację, czy pociąg odjechał już ze stacji (teraz lub w przeszłości).
     * Metoda uwzględnia aktualny czas odjazdu pociągu ze stacji (tzn. potencjalne opóźnienie).
     * @returns {boolean} Wartość logiczna będąca wynikiem sprawdzenia
     */
    hasTrainAlreadyLeft() {
        if (this.isFinalStation()) {
            return this.hasTrainAlreadyArrived()
        }

        const nowMinutes = getNowInMinutesForTargetTimezone()
        const departure = convertTimeToMinutes(this.getCurrentDepartureTime())
        return nowMinutes > departure
    }

    /**
     * Zwraca informację, czy pociąg znajduje się obecnie na stacji, biorąc
     * pod uwagę bieżące czasy przyjazdu i odjazdu.
     * @returns {boolean} Wartość logiczna będąca wynikiem sprawdzenia
     */
    isTrainAtThisStation() {
        return this.hasTrainAlreadyArrived() && !this.hasTrainAlreadyLeft()
    }
}

export { TrainStationData }
