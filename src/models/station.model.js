/**
 * @file Plik zawierający definicję modelu {@link Station}
 * @author Michał Ciężczak
 * @version 1.0.0
 */

/**
 * Klasa reprezentująca stację kolejową.
 */
class Station {
    /**
     * Tworzy instancję klasy z użyciem przekazanych wartości do inicjalizacji
     * prywatnych pól.
     * @param {number} id Unikalne ID stacji
     * @param {string} name Nazwa stacji
     * @param {string} urlName Uproszczona nazwa używana w adresach URL
     * @param {number} popularity Wartość identyfikująca popularność stacji
     */
    constructor(id, name, urlName, popularity) {
        this.id = id
        this.name = name
        this.urlName = urlName
        this.popularity = popularity
    }

    /**
     * Zwraca ID stacji, które zostało przekazane w konstruktorze.
     * @returns {number} ID stacji kolejowej
     */
    getId() {
        return this.id
    }

    /**
     * Zwraca nazwę stacji kolejowej.
     * @returns {string} Nazwa stacji
     */
    getName() {
        return this.name
    }

    /**
     * Zwraca uproszczoną nazwę stacji (niezawierającą niektórych znaków),
     * która stosowana jest w adresach URL.
     * @returns {string} Nazwa stosowana w adresach URL
     */
    getUrlName() {
        return this.urlName
    }

    /**
     * Zwraca wartość oznaczającą popularność stacji kolejowej.
     * Im wyższa wartość, tym popularniejsza stacja.
     * @returns {number} Wartość popularności stacji
     */
    getPopularity() {
        return this.popularity
    }
}

export { Station }
