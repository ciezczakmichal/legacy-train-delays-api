/**
 * @file Plik zawierający definicję modelu {@link StationTrains}
 * @author Michał Ciężczak
 * @version 1.0.0
 */

import { Station } from './station.model'

/**
 * Klasa reprezentująca pociągi dotyczące pojedynczej stacji kolejowej.
 */
class StationTrains {
    /**
     * Tworzy instancję klasy z użyciem przekazanych wartości do inicjalizacji
     * prywatnych pól.
     * @param {Station} station Obiekt stacji kolejowej, której dotyczą pociągi
     * @param {Train[]} trains Tablica pociągów dla stacji kolejowej
     */
    constructor(station, trains) {
        if (!(station instanceof Station)) {
            throw new Error('Stacja - wymagany obiekt klasy')
        }

        if (!Array.isArray(trains)) {
            throw new Error('Argument trains - wymagana tablica pociągów')
        }

        this.station = station
        this.trains = trains
    }

    /**
     * Zwraca obiekt stacji, który został przekazany do konstruktora.
     * @returns {Station} Obiekt stacji kolejowej
     */
    getStation() {
        return this.station
    }

    /**
     * Zwraca tablicę pociągów, która została przekazana do konstruktora.
     * @returns {Train[]} Tablica pociągów dla stacji
     */
    getTrains() {
        return this.trains
    }
}

export { StationTrains }
