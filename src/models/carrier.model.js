/**
 * @file Plik zawierający definicję modelu {@link Carrier}
 * @author Michał Ciężczak
 * @version 1.0.0
 */

/**
 * Klasa reprezentująca przewoźnika kolejowego.
 */
class Carrier {
    /**
     * Tworzy instancję klasy z użyciem przekazanych wartości do inicjalizacji
     * prywatnych pól.
     * @param {number} id Unikalne ID przewoźnika
     * @param {string} name Pełna nazwa przewoźnika
     * @param {string} logoFilename Nazwa pliku wraz z rozszerzeniem, który zawiera logo przewoźnika
     */
    constructor(id, name, logoFilename) {
        this.id = id
        this.name = name
        this.logoFilename = logoFilename
    }

    /**
     * Zwraca ID przewoźnika kolejowego, który identyfikuje go w aplikacji.
     * @returns {number} ID przewoźnika
     */
    getId() {
        return this.id
    }

    /**
     * Zwraca pełną nazwę przewoźnika kolejowego.
     * @returns {string} Pełna nazwa przewoźnika
     */
    getName() {
        return this.name
    }

    /**
     * Zwraca pełną nazwę pliku (wraz z rozszerzeniem) zawierającego logo przewoźnika.
     * @returns {string} Nazwa pliku z logiem przewoźnika
     */
    getLogoFilename() {
        return this.logoFilename
    }
}

export { Carrier }
