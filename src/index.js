import dotenv from 'dotenv'
import express from 'express'
import morgan from 'morgan'
import dayjs from 'dayjs'
import customParseFormat from 'dayjs/plugin/customParseFormat'
import { CronJob } from 'cron'

import routes from './routes/index.routes'

import { iterateOverProviders } from './providers/provider'
import './providers'
import { targetTimezone } from './helpers/date'

dotenv.config()

dayjs.extend(customParseFormat)

const app = express()

app.use(morgan('tiny')) // loguj zapytania HTTP na wyjście
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept'
    )
    next()
})

app.use(routes)

// funkcja logująca błędy
// usunięcie next spowoduje, że uchwyt nie będzie działał dobrze
// eslint-disable-next-line no-unused-vars
app.use(function (err, req, res, next) {
    if (process.env.NODE_ENV !== 'production') {
        console.error(err)
    }

    if (err.status) {
        res.status(err.status).json({ message: err.message })
    } else {
        res.status(500).json({ message: err.message })
    }
})

iterateOverProviders(provider => {
    const dir = provider.getAssetsDir()

    if (typeof dir !== 'string') {
        return
    }

    const name = provider.getProviderName()
    const assetsUrl = `/static/${name}`
    app.use(assetsUrl, express.static(dir))

    const fullAssetsUrl = process.env.SELF_URL_PATH + assetsUrl
    provider.bindAssetsUrl(fullAssetsUrl)
})

// wykonuj codziennie o północy polskiego czasu
const job = new CronJob(
    '0 0 * * *',
    () => {
        console.log(new Date(), 'Wykonywanie provider.doFlushDayData()')
        iterateOverProviders(provider => provider.doFlushDayData())
    },
    null,
    true,
    targetTimezone
)
job.start()

app.listen(process.env.PORT)
