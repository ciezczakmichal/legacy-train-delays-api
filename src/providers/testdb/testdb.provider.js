/**
 * @file Plik zawierający definicję klasy {@link TestDbProvider}, dostarczającej bazę testowych danych
 * @author Michał Ciężczak
 * @version 1.0.0
 */

import path from 'path'
import { Provider } from '../provider'
import { Station } from '../../models/station.model'
import { Carrier } from '../../models/carrier.model'
import { CarrierList } from '../../models/carrierlist.model'
import { StationTrains } from '../../models/stationtrains.model'
import { Train } from '../../models/train.model'
import { TrainStationData } from '../../models/trainstationdata.model'
import { generateAndSetupTrainRandomEvents } from './randomevents'
import { getNowForTargetTimezone } from '../../helpers/date'

const getEarlierTime = (train, stationId) => {
    const data = train.getDataForStation(stationId)
    const a = data.getCurrentArrivalTime() // uwzględnij opóźnienia
    const b = data.getCurrentDepartureTime() // uwzględnij opóźnienia

    if (a === null) {
        return b
    } else if (b === null) {
        return a
    } else {
        return a < b ? a : b
    }
}

/**
 * Klasa dostarczająca funkcjonalność dostawcy danych z bazy testowej.
 * Dziedziczy po klasie {@link Provider}.
 *
 * <pre>
 * Dostawca danych testowych zawiera:
 *  - wszystkie stacje kolejowe w Polsce
 *  - najpopularniejszych pasażerskich przewoźników kolejowych
 *  - bazę połączeń kolejowych na podstawie faktycznie istniejących relacji i nazw
 *  - algorytm generowania opóźnień unikalnych dla każdego z pociągów, każdego dnia
 * </pre>
 */
class TestDbProvider extends Provider {
    /**
     * Zwraca nazwę klasy dostawcy danych.
     * Przeładowuje wersję z klasy nadrzędnej.
     * @see Provider#getProviderName
     * @returns {string} Ciąg "testdb"
     */
    static getProviderName() {
        return 'testdb'
    }

    /**
     * Konstruktor domyślny, tworzy instancję dostawcy danych.
     * Nie wczytuje żadnych danych do pamięci aplikacji (te wczytywane są just-in-time).
     */
    constructor() {
        super()
        this.dataLoaded = false
    }

    /**
     * Załadowuje dane stacji kolejowych do pamięci, dzięki czemu można z nich korzystać.
     * @private
     */
    loadStations() {
        const json = require('./data/stations.json')
        this.stations = json.map(
            s => new Station(s.id, s.name, s.urlName, s.popularity)
        )
    }

    /**
     * Załadowuje dane przewoźników kolejowych do pamięci, dzięki czemu można z nich korzystać.
     * @private
     */
    loadCarriers() {
        const json = require('./data/carriers.json')
        const carriers = json.map(
            c => new Carrier(c.id, c.name, c.logoFilename)
        )

        const logoUrl = `${this.getAssetsUrl()}/carrier_logos/`
        this.carrierList = new CarrierList(carriers, logoUrl)
    }

    /**
     * Załadowuje dane pociągów do pamięci, dzięki czemu można z nich korzystać.
     * Dane ograniczone są do bieżącego dnia.
     * @private
     */
    loadTrains() {
        const todayString = getNowForTargetTimezone().format('YYYYMMDD')
        const json = require('./data/trains.json')

        this.trainList = []
        Object.keys(json).forEach(key => {
            const trainJson = json[key]

            // sprawdź integralność danych
            if (key !== trainJson.id) {
                throw new Error('Błąd integralności danych dla klucza ' + key)
            }

            const carrier = this.carrierList.findCarrier(trainJson.carrierId)

            if (!(carrier instanceof Carrier)) {
                throw new Error(
                    'Nie odnaleziono przewoźnika dla pociągu ' + trainJson.id
                )
            }

            const stations = trainJson.stations.map(
                station =>
                    new TrainStationData(
                        station.id,
                        station.arrivalTime,
                        station.departureTime,
                        station.arrivalTime, // ten sam czas - brak opóźnień
                        station.departureTime // ten sam czas - brak opóźnień
                    )
            )

            const uniqueId = `${todayString}-${trainJson.id}`

            const train = new Train(
                uniqueId,
                carrier,
                trainJson.trainNumber,
                trainJson.trainName,
                trainJson.routeString,
                stations
            )

            // zbuduj ciekawy bieg pociągu...
            generateAndSetupTrainRandomEvents(train)

            this.trainList[train.getId()] = train
        })
    }

    /**
     * Załadowuje wszystkie dane wymagane do pracy metod klasy dostawcy danych,
     * jeżeli nie są już w pamięci.
     * @private
     */
    loadDataIfNeeded() {
        if (this.dataLoaded) {
            return
        }

        this.loadStations()
        this.loadCarriers()
        this.loadTrains()

        this.dataLoaded = true
    }

    /**
     * Zwraca wszystkie stacje kolejowe obecne w bazie testowej.
     * Załadowuje wcześniej dane, jeśli jest taka potrzeba.
     * Metoda implementuje zachowanie z klasy nadrzędnej.
     * @see Provider#getStations
     */
    getStations() {
        this.loadDataIfNeeded()
        return this.stations
    }

    /**
     * Zwraca wszystkich przewoźników kolejowych obecnych w bazie testowej.
     * Załadowuje wcześniej dane, jeśli jest taka potrzeba.
     * Metoda implementuje zachowanie z klasy nadrzędnej.
     * @see Provider#getCarrierList
     */
    getCarrierList() {
        this.loadDataIfNeeded()
        return this.carrierList
    }

    /**
     * Zwraca dane pociągów dla stacji o przekazanym ID.
     * Załadowuje wcześniej dane, jeśli jest taka potrzeba.
     * Metoda implementuje zachowanie z klasy nadrzędnej.
     * @see Provider#getStationTrains
     */
    getStationTrains(stationId) {
        this.loadDataIfNeeded()
        const station = this.stations.find(s => s.getId() === stationId)

        if (!station) {
            throw new Error('Nierozpoznana stacja')
        }

        /* Zwróć pociągi, które zawierają wskazaną stację na trasie */

        let trains = Object.values(this.trainList).filter(train =>
            train.hasStationInRoute(stationId)
        )

        /* Zwróć tylko te, które dopiero przyjadą lub nie odjechały */

        trains = trains.filter(
            train => !train.getDataForStation(stationId).hasTrainAlreadyLeft()
        )

        /* Posortuj rosnąco według czasu przyjazdu / wyjazdu (mniejszego z nich) */

        trains = trains.sort((a, b) => {
            const aTime = getEarlierTime(a, stationId)
            const bTime = getEarlierTime(b, stationId)

            return aTime === bTime ? 0 : aTime < bTime ? -1 : 1
        })

        return new StationTrains(station, trains)
    }

    /**
     * Zwraca dane pociągu o wskazanym ID.
     * Załadowuje wcześniej dane, jeśli jest taka potrzeba.
     * Metoda implementuje zachowanie z klasy nadrzędnej.
     * @see Provider#getTrain
     */
    getTrain(trainId) {
        this.loadDataIfNeeded()

        const train = this.trainList[trainId] || null

        if (!(train === null || train instanceof Train)) {
            throw new Error('Nieznany pociąg')
        }

        return train
    }

    /**
     * Zwraca lokalizację katalogu, który zawiera statyczne dane do udostępnienia
     * przez serwer aplikacji.
     * Klasa udostępnia loga przewoźników.
     * @see Provider#getAssetsDir
     */
    getAssetsDir() {
        return path.join(__dirname, 'assets')
    }

    /**
     * Implementuje mechanizm oczyszczania bazy połączeń kolejowych i jej
     * regeneracji dla bieżącego dnia.
     * @see Provider#doFlushDayData
     */
    doFlushDayData() {
        if (this.dataLoaded) {
            this.trainList = []
            this.loadTrains()
        }
    }
}

export { TestDbProvider }
