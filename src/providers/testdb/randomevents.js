/**
 * @file Plik pomocniczy dostawcy danych testowych
 * @author Michał Ciężczak
 * @version 1.0.0
 */

import seedrandom from 'seedrandom'

const between = (x, min, max) => x >= min && x <= max

const isIntercity = train => between(train.getCarrier().getId(), 200, 299)

const generateAndSetupTrainRandomEvents = train => {
    const id = train.getId()
    const rng = seedrandom(id) // w pełni powtarzalne (i unikalne) źródło liczb losowych
    const randomInt = maxExclusive => Math.abs(rng.int32() % maxExclusive)

    const randomDelay = (noDelays, tenMinutes, oneHour) => {
        const percent = randomInt(100)

        if (percent < noDelays) {
            return 0 // brak opóźnienia
        } else if (between(percent, noDelays, tenMinutes - 1)) {
            return randomInt(10) // opóźnienie do 10 minut
        } else if (between(percent, tenMinutes, oneHour)) {
            return randomInt(50) + 10 // opóźnienie do godziny
        } else {
            return randomInt(180) + 60 // opóźnienie do 4h
        }
    }

    const stationCount = train.stationsCount()
    const fatalStationIndex = randomInt(stationCount)
    let delay = 0

    // król opóźnień :)
    if (train.getTrainName().toLowerCase() === 'malczewski') {
        delay = randomInt(200) + 30
    } else {
        delay = isIntercity(train)
            ? randomDelay(30, 70, 90)
            : randomDelay(45, 90, 100)
    }

    if (delay === 0) {
        return
    }

    // ile minut pociąg może nadrobić na trasie pomiędzy stacjami
    const maxMinutesToCatchUpOnTheWay = isIntercity(train) ? 7 : 3

    const generateRealisticDelayFlow = fromIndex => {
        // niech istnieje niewielka szansa na uzyskanie opóźnienia na stacji
        let skipApplyArrivalDelay =
            !train.getDataForStationIndex(fatalStationIndex).isFinalStation() &&
            randomInt(10) <= 2

        for (let i = fromIndex; i < stationCount; ++i) {
            const station = train.getDataForStationIndex(i)
            let arrivalTime = null

            if (!station.isFirstStation()) {
                arrivalTime = station.getArrivalTimeInMinutes()

                if (!skipApplyArrivalDelay) {
                    station.setCurrentArrivalTimeInMinutes(arrivalTime + delay)
                }
            }

            // gdyby stacja była pierwszą w rozkładzie, nie stosuj opóźnienia przyjazdowego na następnej
            skipApplyArrivalDelay = false

            if (!station.isFinalStation()) {
                const departureTime = station.getDepartureTimeInMinutes()

                if (arrivalTime !== null) {
                    const stationBuffer = departureTime - arrivalTime

                    // pozwól nadrobić czas na stacji
                    if (stationBuffer > (isIntercity(train) ? 2 : 1)) {
                        delay -= randomInt(stationBuffer)
                    }
                }

                station.setCurrentDepartureTimeInMinutes(departureTime + delay)
            }

            delay -= randomInt(maxMinutesToCatchUpOnTheWay)

            if (delay <= 0) {
                break
            }
        }
    }

    generateRealisticDelayFlow(fatalStationIndex)
}

export { generateAndSetupTrainRandomEvents }
