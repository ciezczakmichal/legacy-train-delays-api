/**
 * @file Plik zawierający definicję klasy {@link Provider} oraz funkcje pomocnicze
 * @author Michał Ciężczak
 * @version 1.0.0
 */

/**
 * Klasa bazowa dostawcy danych dla aplikacji.
 * Klasy pochodne implementując metody dostarczają usłudze określone
 * dane (np. listę przewoźników, pociągi na stacji), ukrywając
 * szczegóły procesu ich pozyskiwania.
 * @hideconstructor
 */
class Provider {
    /**
     * Zwraca unikalną nazwę dostawcy danych jako string.
     * Korzysta z metody statycznej o tej samej nazwie
     *
     * @returns {string} Nazwa klasy dostawcy danych
     */
    getProviderName() {
        // nazwa ze statycznej metody
        return this.constructor.getProviderName()
    }

    /**
     * Przypisuje adres dostępu do statycznych zasobów udostępnianych przez dostawcę danych.
     * Metoda wykorzystywana przez kod operujący na obiektach klasy Provider.
     *
     * @param {string} assetsUrl
     */
    bindAssetsUrl(assetsUrl) {
        this.assetsUrl = assetsUrl
    }

    /**
     * Zwraca adres dostępu do statycznych zasobów udostępnianych przez dostawcę danych.
     * Metoda wykorzystywana przez kod operujący na obiektach klasy Provider.
     *
     * @returns {string} Adres dostępu do statycznych zasobów
     */
    getAssetsUrl() {
        return this.assetsUrl
    }

    /* Metody do przeładowania w klasach pochodnych */

    /**
     * Zwraca unikalną nazwę dostawcy danych jako string.
     * Metoda statyczna przeładowywana w klasach pochodnych.
     *
     * @returns {string} Nazwa klasy dostawcy danych
     * @abstract
     */
    static getProviderName() {}

    /**
     * Zwraca tablicę obiektów zawierającą wszystkie stacje kolejowe
     * wspierane przez dostawcę danych.
     * Metoda implementowana w klasach pochodnych.
     *
     * @returns {Station[]} Tablica obiektów {@link Station}
     * @abstract
     */
    getStations() {}

    /**
     * Zwraca obiekt klasy {@link CarrierList}, który zawiera bazę przewoźników kolejowych
     * dostawcy danych.
     * Metoda implementowana w klasach pochodnych.
     *
     * @returns {CarrierList} Obiekt reprezentujący bazę przewoźników
     * @abstract
     */
    getCarrierList() {}

    /**
     * Zwraca dane pociągów dla wskazanej stacji. Wynikiem metody jest obiekt klasy {@link StationTrains}
     * zawierający wszystkie pociągi, które mają wskazaną stację na swojej trasie przejazdu.
     * Zwracane dane powinny być ograniczone do bieżącego dnia i nie zawierać pociągów,
     * które opuściły już stację.
     * Kolejność pociągów na liście powinna być w porządku rosnącym wg czasu przyjazdu lub wyjazdu.
     * Jeśli stacja o przekazanym ID nie istnieje, metoda rzuca wyjątek.
     * Metoda implementowana w klasach pochodnych.
     *
     * @param {number} stationId ID stacji, dla której pobrać dane
     * @returns {StationTrains} Dane pociągów dla stacji
     * @abstract
     */
    // eslint-disable-next-line no-unused-vars
    getStationTrains(stationId) {}

    /**
     * Zwraca dane pociągu o wskazanym ID. Jeśli ID nie wskazuje na istniejący pociąg,
     * metoda powinna rzucić wyjątek.
     * Metoda implementowana w klasach pochodnych.
     *
     * @param {number} trainId ID pociągu, którego dane mają być pobrane
     * @returns {Train} Dane wskazanego pociągu
     * @abstract
     */
    // eslint-disable-next-line no-unused-vars
    getTrain(trainId) {}

    /**
     * Zwraca ścieżkę do katalogu, w którym dostawca danych przechowuje dane statyczne,
     * które powinny być udostępniane przez serwer aplikacji.
     * Jeśli dostawca danych nie udostępnia zasobów, powinien zwrócić NULL.
     * Metoda implementowana w klasach pochodnych.
     *
     * @returns {string|null} Ścieżka do katalogu lub NULL
     * @abstract
     */
    getAssetsDir() {}

    /**
     * Metoda wywoływana przez aplikację codziennie o północy, służąca do wykonania konserwacji.
     * Jest przeznaczona do przebudowy bazy danych dostawców danych, np. usunięcia informacji o
     * pociągach z poprzedniego dnia.
     * Metoda implementowana w klasach pochodnych.
     *
     * @abstract
     */
    doFlushDayData() {}
}

let providers = []
let defaultProvider = null

/**
 * Rejestruje wskazany obiekt jako dostawcę danych.
 * Obiekt powinien dziedziczyć po klasie {@link Provider}, w innym wypadku funkcja rzuci wyjątek.
 *
 * @param {Provider} p Obiekt klasy dostawcy danych, który ma być zarejestrowany
 */
const registerProvider = p => {
    if (!(p instanceof Provider)) {
        throw new Error(
            'Klasa dostawcy danych musi być pochodną klasy Provider'
        )
    }

    providers[p.getProviderName()] = p
}

/**
 * Przypisuje domyślnego dostawcę danych dla aplikacji na podstawie jego nazwy.
 * Jeśli dostawca o takiej nazwie nie może być odnaleziony, funkcja rzuca wyjątek.
 *
 * @param {string} providerName Nazwa domyślnego dostawcy danych
 */
const registerDefaultProvider = providerName => {
    if (typeof providerName !== 'string') {
        throw new Error('Nazwa dostawcy musi być stringiem')
    }

    if (!(providerName in providers)) {
        throw new Error('Dostawca o takiej nazwie nie został zarejestrowany')
    }

    defaultProvider = providerName
}

/**
 * Zwraca instancję domyślnego dostawcy danych dla aplikacji.
 * Zwracany obiekt implementuje klasę {@link Provider}.
 * Rzuca wyjątek, jeśli nie przypisano domyślnego dostawcy danych.
 *
 * @returns {Provider} Obiekt domyślnego dostawcy danych
 */
const getDefaultProvider = () => {
    if (defaultProvider === null) {
        throw new Error('Nie określono domyślnego dostawcy danych')
    }

    return providers[defaultProvider]
}

/**
 * Iteruje po bazie dostawców danych i wykonuje przekazaną funkcję callback
 * na każdym z obiektów w bazie.
 *
 * @param {function(Provider): void} callback Funkcja, która ma być wykonana z argumentem z dostawcą danych
 */
const iterateOverProviders = callback => {
    Object.keys(providers).forEach(key => callback(providers[key]))
}

export {
    Provider,
    registerProvider,
    registerDefaultProvider,
    getDefaultProvider,
    iterateOverProviders,
}
