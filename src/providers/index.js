import { registerProvider, registerDefaultProvider } from './provider'
import { TestDbProvider } from './testdb/testdb.provider'

registerProvider(new TestDbProvider())

registerDefaultProvider(TestDbProvider.getProviderName())
