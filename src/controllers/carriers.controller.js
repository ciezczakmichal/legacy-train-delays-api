/**
 * @file Plik zawierający definicję kontrolera, klasę {@link CarriersController}
 * @author Michał Ciężczak
 * @version 1.0.0
 */

import { getDefaultProvider } from '../providers/provider'
import { CarrierList } from '../models/carrierlist.model'

/**
 * Kontroler odpowiedzialny za obsługę zapytań dotyczących przewoźników kolejowych.
 * @hideconstructor
 */
class CarriersController {
    /**
     * Zwraca dane wszystkich przewoźników kolejowych w formacie JSON z zachowaniem
     * struktury oczekiwanej dla warstwy interfejsu użytkownika.
     * Wykorzystuje domyślnego dostawcę danych jako źródło danych.
     *
     * @returns {Promise<Object>}
     */
    async getCarriers() {
        const provider = getDefaultProvider()
        const carriers = provider.getCarrierList()

        if (!(carriers instanceof CarrierList)) {
            throw new Error('Niepoprawny format danych od dostawcy danych')
        }

        return {
            carriers: {
                logoBaseUrl: carriers.getLogoBaseUrl(),
                data: carriers.getCarriers().map(carrier => ({
                    id: carrier.getId(),
                    name: carrier.getName(),
                    logoFilename: carrier.getLogoFilename(),
                })),
            },
        }
    }
}

export { CarriersController }
