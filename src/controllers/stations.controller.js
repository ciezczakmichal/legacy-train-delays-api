/**
 * @file Plik zawierający definicję kontrolera, klasę {@link StationsController}
 * @author Michał Ciężczak
 * @version 1.0.0
 */

import { getDefaultProvider } from '../providers/provider'
import { StationTrains } from '../models/stationtrains.model'
import { TrainsController } from './trains.controller'

/**
 * Kontroler odpowiedzialny za obsługę zapytań dotyczących stacji kolejowych.
 * @hideconstructor
 */
class StationsController {
    /**
     * Zwraca dane wszystkich stacji kolejowych.
     * Zwracane dane są w formacie JSON z zachowaniem
     * struktury oczekiwanej dla warstwy interfejsu użytkownika.
     * Wykorzystuje domyślnego dostawcę danych jako źródło danych.
     *
     * @returns {Promise<Object>}
     */
    async getStations() {
        const provider = getDefaultProvider()
        const stations = provider.getStations()

        if (!Array.isArray(stations)) {
            throw new Error('Niepoprawny format danych od dostawcy danych')
        }

        return {
            stations: stations.map(station => ({
                id: station.getId(),
                name: station.getName(),
                urlName: station.getUrlName(),
                popularity: station.getPopularity(),
            })),
        }
    }

    /**
     * Zwraca dane pociągów pasażerskich dla stacji kolejowej o wskazanym ID.
     * Zwracane dane są w formacie JSON z zachowaniem
     * struktury oczekiwanej dla warstwy interfejsu użytkownika.
     * Wykorzystuje domyślnego dostawcę danych jako źródło danych.
     *
     * @returns {Promise<Object>}
     */
    async getStationTrains(stationIdString) {
        const provider = getDefaultProvider()
        const id = parseInt(stationIdString, 10)
        const trains = provider.getStationTrains(id)

        if (!(trains instanceof StationTrains)) {
            throw new Error('Niepoprawny format danych od dostawcy danych')
        }

        const station = trains.getStation()

        return {
            station: {
                id: station.getId(),
                name: station.getName(),
                urlName: station.getUrlName(),
            },
            trains: trains.getTrains().map(train => ({
                id: train.getId(),
                carrierId: train.getCarrier().getId(),
                trainNumber: train.getTrainNumber(),
                trainName: train.getTrainName(),
                routeString: train.getRouteString(),
                dataForStation: TrainsController.createStationDataJsonResponse(
                    train.getDataForStation(id)
                ),
            })),
        }
    }
}

export { StationsController }
