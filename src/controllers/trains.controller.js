/**
 * @file Plik zawierający definicję kontrolera, klasę {@link TrainsController}
 * @author Michał Ciężczak
 * @version 1.0.0
 */

import { getDefaultProvider } from '../providers/provider'
import { Train } from '../models/train.model'

/**
 * Kontroler odpowiedzialny za obsługę zapytań dotyczących pojedynczych pociągów.
 * @hideconstructor
 */
class TrainsController {
    /**
     * Zwraca dane pociągu o wskazanym ID.
     * Zwracane dane są w formacie JSON z zachowaniem
     * struktury oczekiwanej dla warstwy interfejsu użytkownika.
     * Wykorzystuje domyślnego dostawcę danych jako źródło danych.
     *
     * @returns {Promise<Object>}
     */
    async getTrain(trainId) {
        const provider = getDefaultProvider()
        const train = provider.getTrain(trainId)

        if (train === null) {
            return null
        } else if (!(train instanceof Train)) {
            throw new Error('Niepoprawny format danych od dostawcy danych')
        }

        return {
            id: train.getId(),
            carrierId: train.getCarrier().getId(),
            trainNumber: train.getTrainNumber(),
            trainName: train.getTrainName(),
            routeString: train.getRouteString(),
            hasStartedRoute: train.hasStartedRoute(),
            hasFinishedRoute: train.hasFinishedRoute(),
            timeline: train
                .getStationsData()
                .map(sd => TrainsController.createStationDataJsonResponse(sd)),
        }
    }

    /**
     * Pomocnicza metoda klasy, która koduje dane pojedynczej
     * stacji na trasie pociągu do obiektu JSON o stałej strukturze.
     * @param {TrainStationData} stationData Dane stacji na trasie pociągu
     */
    static createStationDataJsonResponse(stationData) {
        const sd = stationData

        return {
            stationId: sd.getStationId(),
            arrivalTime: sd.getArrivalTime(),
            departureTime: sd.getDepartureTime(),
            currentArrivalTime: sd.getCurrentArrivalTime(),
            currentDepartureTime: sd.getCurrentDepartureTime(),
            arrivalDelay: sd.getArrivalDelay(),
            departureDelay: sd.getDepartureDelay(),
            alreadyArrived: sd.hasTrainAlreadyArrived(),
            alreadyLeft: sd.hasTrainAlreadyLeft(),
        }
    }
}

export { TrainsController }
