/**
 * @file Definicja funkcji i stałych pomocniczych w konwersji czasu.
 * @author Michał Ciężczak
 * @version 1.0.0
 */

import dayjs from 'dayjs'

/**
 * Strefa czasowa, w której operują dostawcy danych kolejowych (Polska).
 * Może być różna od strefy czasowej systemu operacyjnego.
 */
const targetTimezone = 'Europe/Warsaw'

/**
 * Zwraca obiekt biblioteki dayjs reprezentujący aktualny czas systemowy
 * przekonwertowany do docelowej strefy czasowej (stała {@link targetTimezone}).
 * @returns {dayjs} Obiekt daty i czasu
 */
const getNowForTargetTimezone = () =>
    dayjs(new Date().toLocaleString('en-US', { timeZone: targetTimezone }))

/**
 * Zwraca liczbę minut, która upłynęła od północy do obecnego momentu
 * w docelowej strefie czasowej (stała {@link targetTimezone}).
 * @returns {number} Liczba minut, która upłynęła od północy
 */
const getNowInMinutesForTargetTimezone = () => {
    const now = getNowForTargetTimezone()
    return now.hour() * 60 + now.minute()
}

export {
    targetTimezone,
    getNowForTargetTimezone,
    getNowInMinutesForTargetTimezone,
}
