import { Router } from 'express'
import { StationsController } from '../controllers/stations.controller'

const router = Router()
const controller = new StationsController()

router.get('/', async (req, res, next) => {
    await controller
        .getStations()
        .then(data => res.json(data))
        .catch(next)
})

router.get('/:id/trains', async (req, res, next) => {
    await controller
        .getStationTrains(req.params.id)
        .then(data => res.json(data))
        .catch(next)
})

export default router
