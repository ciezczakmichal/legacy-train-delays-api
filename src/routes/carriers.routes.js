import { Router } from 'express'
import { CarriersController } from '../controllers/carriers.controller'

const router = Router()
const controller = new CarriersController()

router.get('/', async (req, res, next) => {
    await controller
        .getCarriers()
        .then(data => res.json(data))
        .catch(next)
})

export default router
