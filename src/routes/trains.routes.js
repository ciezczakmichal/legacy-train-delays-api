import { Router } from 'express'
import { TrainsController } from '../controllers/trains.controller'

const router = Router()
const controller = new TrainsController()

router.get('/:id', async (req, res, next) => {
    await controller
        .getTrain(req.params.id)
        .then(data => (data === null ? res.sendStatus(404) : res.json(data)))
        .catch(next)
})

export default router
