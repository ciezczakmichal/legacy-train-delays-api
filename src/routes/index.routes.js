import { Router } from 'express'
import stations from './stations.routes'
import carriers from './carriers.routes'
import trains from './trains.routes'

let router = Router()

router.use('/stations', stations)
router.use('/carriers', carriers)
router.use('/trains', trains)

let prefix = process.env.API_PREFIX

if (prefix) {
    router = new Router().use(prefix, router)
}

export default router
